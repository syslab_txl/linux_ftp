#ifndef __FILE_LIST__
#define __FILE_LIST__
/**
*已打开的文件资源表，动态维护
*/
#include <vector>
#include <sys/stat.h>

using std::vector;

#define fileNameLen 100

typedef struct FILE_LIST_ITEM
{
	char filePath[fileNameLen];
	int fileId;
	struct stat fileData;
	int savedBlockLen;
	int BlockTotalLen;
}FILE_LIST_ITEM;

extern vector<FILE_LIST_ITEM> FILE_LIST_VECTOR;

//判断文件是否已经在列表内
int fileIn(const char* filePath);
//添加文件，返回文件ID
int addFile(const char* fpath,int blockTotalLen);
//更新文件数据，curBlockNo置-1则从列表中删除此文件信息
int updateFile(int fileId,int curBlockNo);
//从文件列表中删除 
int delFile(int fileId);//blockTotalLen
//由fileId从列表中取得文件索引
int getFromFileId(int fileId);
#endif
