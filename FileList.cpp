#include "FileList.h"
#include "ErrorCode.h"
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

vector<FILE_LIST_ITEM> FILE_LIST_VECTOR;

//判断文件是否已经在列表内
int fileIn(const char* filePath)
{
	for(int i=0;i<FILE_LIST_VECTOR.size();i++)
	{
		if(strcmp(filePath,FILE_LIST_VECTOR[i].filePath)==0)
			return FILE_LIST_VECTOR[i].fileId;
	}
	return -1;
}
//添加文件，返回文件ID
int addFile(const char* fpath,int blockTotalLen)
{
	int file = open(fpath,O_WRONLY|O_APPEND|O_CREAT,S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH);
	if(file==-1)
		return -1;
	struct stat st;
	stat(fpath,&st);
	struct FILE_LIST_ITEM fitem = {"",file,st,0,blockTotalLen};
	strcpy(fitem.filePath,fpath);	
	FILE_LIST_VECTOR.push_back(fitem);
	return file;
}

//更新文件数据，curBlockNo置-1则从列表中删除此文件信息
int updateFile(int fileId,int curBlockNo)
{
	int index_i = getFromFileId(fileId);
	if(index_i==-1)
		return -1;
	/*if(curBlockNo == -1)
	{
		delFile(fileId);
		close(fileId);
		return -1;
	}*/	
	//如果文件最后一个数据块成功写入，则从文件列表中删除此文件信息
	if(curBlockNo == FILE_LIST_VECTOR[index_i].BlockTotalLen)
	{
		char newName[fileNameLen] = {'\0'};
		char* oldName = FILE_LIST_VECTOR[index_i].filePath;
		int slen = strlen(oldName);
		strncpy(newName,oldName,slen-4);
		rename(oldName,newName);
		delFile(fileId);
		close(fileId);
		return EOF;
	}
	else
		FILE_LIST_VECTOR[index_i].savedBlockLen = curBlockNo;
	return SUCCESS;
}
//从文件列表中删除 
int delFile(int fileId)
{
	int index_i = getFromFileId(fileId);
	if(index_i == -1)
		return 0;
	vector<FILE_LIST_ITEM>::iterator ite = FILE_LIST_VECTOR.begin();
	ite += index_i;
	FILE_LIST_VECTOR.erase(ite);
	return 1;
}
//由fileId从列表中取得文件索引
int getFromFileId(int fileId)
{
	int index_i;
	for(index_i=0;index_i<FILE_LIST_VECTOR.size();index_i++)
	{
		if(FILE_LIST_VECTOR[index_i].fileId==fileId)
			break;
	}
	if(index_i>=FILE_LIST_VECTOR.size())
		return -1;
	return index_i;
}
