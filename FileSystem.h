#ifndef __FILE_SYSTEM__
#define __FILE_SYSTEM__

#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string>

#include "ErrorCode.h"
#include "ftpDataBlock.h"
#include "FileList.h"

//CONSTANT DEF

#define DATABLOCK_LEN dataBlockSize

//50MB per segment
#define DATABLOCK_NUM_PER_SEGMENT (1024*50)
#define SEGMENT_SIZE DATABLOCK_LEN*DATABLOCK_NUM_PER_SEGMENT

typedef struct DataPack
{
	int fileId;//文件ID
	int fileSize;//文件大小
	int segmentNum;//分块读取的段落数目
	int totalBlockNum;//总数据包数
	int restBlockNum;//剩余数据包数
	int bufferLen;//data长度
	DataBlock* data;
}DataPack;

//CONSTANT DEF END

using namespace std;
//MODE
//transfer mode
enum trans_mode{S=0,B,C};
//trans_mode cur_mode;
trans_mode Mode(trans_mode);

//List
int isDir(char* path);
void printStat(struct stat* pStat);
void List(char* path);

//PWD	cur directory
//string CUR_DIR;
string PWD();

//QUIT

int QUIT();

//RETR
int RETR();

//STORE
int STORE(char* path,DataBlock* data);
//INI FilePack
DataPack* INI_FILE_BUFFER(char* path);
//读取文件并分块,return LOCAL_ERR , SUCCESS ,EOF(while end)
int READ_FILE_BUFFER(DataPack* dataPack);
//buferrNum
int CLOSE_FILE_BUFFER(DataPack* dataPack);

#endif

