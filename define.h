


#define SERVER_PORT    21 
#define LENGTH_OF_LISTEN_QUEUE 20
#define BUFF_SIZE 512
#define DATE_BUFF_SIZE 1024
#define ADD_SIZE 16
#define MAXPATHLEN 512
#define SPACE  " "
#define PORTDELIM  ","
#define UNFINISHED "PORT还没完成"

enum { 
	CMD_ERR=-1,// By why
	CMD_CLOSE=0,// By why
	CMD_ABOR=1,//中断
	CMD_CWD,//cd 
	CMD_DELE,//rm  (file)
	CMD_LIST,//ls
	CMD_MKD,//mkdir
	CMD_PASS,//password
	CMD_QUIT,//quit
	CMD_RETR,//get
	CMD_STOR,//put
	CMD_USER,//user
	CMD_PORT,//port and address  when recive and post data  (get put and list)
	CMD_SYST,//sysytem
	CMD_UNKNOWN
 };




#define REPL_220 "220 服务就绪\n"
#define REPL_332 "332 输入帐号\n"
#define REPL_331 "331 输入密码\n"
#define REPL_230 "230 登录成功\n"
#define REPL_530 "530 未登录网络\n"

#define REPL_150 "150 打开连接\n"
#define REPL_425 "425 无法打开连接\n"
#define REPL_426 "426 结束连接\n"
#define REPL_221 "221 结束数据连接\n"
#define REPL_200 "200 命令执行成功\n"
#define REPL_250 "250 文件操作完成\n"
#define REPL_257 "257 新建文件夹完成\n"
#define REPL_451 "451 本地错误\n"


#define REPL_500 "500 无效命令\n"
#define REPL_501 "501 错误参数\n"
#define REPL_502 "502 命令没有执行\n"
#define REPL_503 "503 错误指令序列\n"
#define REPL_226 "226 退出网络\n"

#define REPL_215 "215 Linux\n"



