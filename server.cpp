
#include "define.h"
#include "head.h"


int send_repl(int socket,const char * msg){
	if (send(socket,msg,strlen(msg),0)< 0) { 
		close(socket);
		exit(0);
	}
	return 0;
}

int getCMD(int socket, char * cmd_buff){
	char rcv_buff[BUFF_SIZE];
	memset((char *)&rcv_buff, 0, BUFF_SIZE);
	rcv_buff[0]='\0';
	if (recv(socket,rcv_buff,BUFF_SIZE,0)<1){
		//close(socket);
		return CMD_ERR;
	}

	cmd_buff[0]='\0';
	//get cmd and buff from  rcv_buff
	// ex.  rcv_buff  is  USER xjp
	//return USER and cmd_buff is xjp
	char *ss=rcv_buff;
	printf("%s\n",ss);

	char * cmd=strtok(rcv_buff,SPACE);//get USER
	char * arg = strtok(NULL,SPACE);
	if(arg!=NULL)
		strcpy(cmd_buff,arg);//get xjp
	//printf("%s\n",cmd_buff);
	switch(cmd[0]){
		case 'A':return CMD_ABOR;
		case 'C':return CMD_CWD;
		case 'D':return CMD_DELE;
		case 'L':return CMD_LIST;
		case 'M':return CMD_MKD;
		case 'P':
			switch(cmd[1]){
				case 'A':return CMD_PASS;
				case 'O':return CMD_PORT;
				default :return CMD_UNKNOWN;
			}
		case 'Q':return CMD_QUIT;
		case 'R':return CMD_RETR;
		case 'S':
			switch(cmd[1]){
				case 'T':return CMD_STOR;
				case 'Y':return CMD_SYST;
				default :return CMD_UNKNOWN;
			}
		case 'U':return CMD_USER;
		default :return CMD_UNKNOWN;
	}

}


int create_data_socket(int sock_fd,int client_port, const char* client_add){

    if(client_port<1) {
		send_repl(sock_fd,REPL_425);
		return -1;
	}
	int sock=-1;
	struct sockaddr_in clieaddr;
	clieaddr.sin_family = AF_INET;
	clieaddr.sin_addr.s_addr = inet_addr(client_add);
	clieaddr.sin_port = htons (client_port);
	if ((sock = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		send_repl(sock,REPL_425);
		return -1;
	}
	if(connect (sock, (struct sockaddr *)&clieaddr, sizeof (clieaddr))!=0) {
		send_repl(sock,REPL_425);
		return -1;
	}
	return sock;
}


int main(int argc, char **argv)
{
/*create socket  begin*/
	
    struct sockaddr_in server_addr;
    bzero(&server_addr,sizeof(server_addr)); 
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(SERVER_PORT);
    int server_socket = socket(PF_INET,SOCK_STREAM,0);
    if( server_socket < 0){
        printf("Create Socket Failed!");
        exit(0);
    }
	{ 
	   int opt =1;
	   setsockopt(server_socket,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));
	}
	    
    if( bind(server_socket,(struct sockaddr*)&server_addr,sizeof(server_addr))){
        printf("Server Bind Port : %d Failed!", SERVER_PORT); 
        exit(0);
    }

    if ( listen(server_socket, LENGTH_OF_LISTEN_QUEUE) )
    {
        printf("Server Listen Failed!"); 
        exit(0);
    }
    printf("Im waiting!! \n");
/*create socket  end*/


/*begin receive */
    int client_port=0;
    char  client_add[ADD_SIZE]={'\0'};
    bool is_loged=false;

    char cur_dir[MAXPATHLEN];
	memset((char *)&cur_dir, 0, MAXPATHLEN);
	

    struct sockaddr_in client_addr;
    socklen_t length = sizeof(client_addr);
    int client_socket = accept(server_socket,(struct sockaddr*)&client_addr,&length);
    if ( client_socket < 0){
        printf("Server Accept Failed!\n");
    }
	send_repl(client_socket,REPL_220);
	int i=0;
	char user_name[100]={'\0'};//
    while (1){
    	char cmd_buff[BUFF_SIZE]={'\0'};
    	//memset((char *)&cmd_buff, 0, BUFF_SIZE);
    	//cmd_buff[0]='\0';
    	int command=getCMD(client_socket,cmd_buff);
    	switch(command){
    		case CMD_ABOR:
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					if(client_socket!=-1){
						close(client_socket);
					} 
					send_repl(client_socket,REPL_226);
				} 
    			break;
    		case CMD_CWD:  //cd 
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					cd(client_socket,cmd_buff);
				}
    			break;
    		case CMD_DELE: //rm file
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					rm(client_socket,cmd_buff);
				}
    			break;
    		case CMD_MKD://mkdir
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					mkdir(client_socket,cmd_buff);
				}
    			break;


    		// when data post 
    		case CMD_PORT://..  before ls  get and put
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					client_port=port(client_socket,cmd_buff,client_add);
					if(client_port<0) {
						send_repl(client_socket,REPL_501);
						client_port = 0;
					} else {
						send_repl(client_socket,REPL_200);
					}
				}
    			break;
    		case CMD_LIST://ls
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					int data_socket=create_data_socket(client_socket,client_port,client_add); 
					printf("data_socketdata_socketdata_socketdata_socket\n");
					if(data_socket>0) {
						if (cmd_buff==NULL){
							ls(client_socket,data_socket,cur_dir);
						}else{
							ls(client_socket,data_socket,cmd_buff);
						}
					}else{
						send_repl(client_socket,REPL_425);
					}
				}
			
    			break;
    		case CMD_RETR://get 
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					int data_socket=create_data_socket(client_socket,client_port,client_add); 
					
					if(data_socket!=0) {
						get(client_socket,data_socket,cmd_buff,cur_dir);
					}else{
						close(data_socket);
						send_repl(client_socket,REPL_425);
					}
					send_repl(client_socket,REPL_502);
				}
    			break;
    		case CMD_STOR://put
    			if(!is_loged) send_repl(client_socket,REPL_530);
				else {
					int data_socket=create_data_socket(client_socket,client_port,client_add); 
					
					if(data_socket!=0) {
						put(client_socket,data_socket,cmd_buff,cur_dir);
					}else{
						close(data_socket);
						send_repl(client_socket,REPL_425);
					}
					send_repl(client_socket,REPL_502);
				}
    			break;

    		// when log in
    		//用户名和密码统一验证
    		case CMD_USER:  //
    			/*if (user_exit(cmd_buff)){ 
					send_repl(client_socket,REPL_331);
					strcpy(user_name,cmd_buff);
				}else send_repl(client_socket,REPL_332);*/
				send_repl(client_socket,REPL_331);
				strcpy(user_name,cmd_buff);
    			break;
    		case CMD_PASS: //
    			//By why 2014/1/27
    			if(true)//login(user_name,cmd_buff))
	    		{	
	    			printf("Login Succeeded\n");
	    			is_loged=true;
	    			strcpy(cur_dir,"/home");
	    			send_repl(client_socket,REPL_230);
    				chdir(cur_dir);
					if((getcwd(cur_dir,MAXPATHLEN)==NULL)) {
						send_repl(client_socket,REPL_501);
						close(client_socket);
					}
	    		}
	    		else
	    		{
	    			printf("Login Failed\n");
	    			send_repl(client_socket,REPL_503);
	    			//close(client_socket);
	    		}
	    			
    			break;
    		case CMD_QUIT:
    			close(client_socket);
    			exit(0);
    			break;
    		case CMD_SYST: 
    			//if(is_loged)
    			send_repl(client_socket,REPL_215);
    			break;
    		case CMD_UNKNOWN:
    			send_repl(client_socket,REPL_500);
    			break;
    		//By why
    		case CMD_ERR:
    		case CMD_CLOSE:
    			break;
    		default:
    			send_repl(client_socket,REPL_502);

    	}
    } 
    close(client_socket);
    close(server_socket);
    return 0;
}
