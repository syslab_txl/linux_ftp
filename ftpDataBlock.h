/* 
 * File:   ftpDataBlock.h
 * Author: Liu Yajun
 *
 * Created on 2013年12月15日, 下午7:37
 */

#ifndef FTPDATABLOCK_H
#define	FTPDATABLOCK_H

typedef unsigned char Byte;
#define dataBlockSize 1024      //数据块大小1024Byte

typedef struct{
    int blockNO;        //数据块序号
    int length;         //数据块中数据的长度
    int blockTotalLen;
    Byte data[dataBlockSize];
}DataBlock;

#endif	/* FTPDATABLOCK_H */

