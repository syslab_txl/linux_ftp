#include "define.h"
#include "head.h"
#include "FileSystem.h"

/*bool user_exit(char * username){
	spwd* sp = getspnam(username);
	printf("user_exituser_exituser_exit\n");
	if(sp==NULL)return false;
	return true;
}*/

bool login(char * username,const char * password){
	spwd* sp = getspnam(username);
	printf("loginloginlogin\n");
	if(sp==NULL)return false;
	if(strcmp(sp->sp_pwdp,crypt(password,sp->sp_pwdp))!=0)
		return false;
	return true;
}


int cd(int client_socket,const char *cmd_buffer){

	if(chdir(cmd_buffer)==0){
		send_repl(client_socket,REPL_501);
		return -1;
	}

	send_repl(client_socket,REPL_250);
	return 0;
}

int rm(int client_socket,const char *cmd_buffer){

	if( remove(cmd_buffer)==0 ){
		send_repl(client_socket,REPL_501);
		return -1;
	}
	send_repl(client_socket,REPL_250);
	return 0;
}


int mkdir(int client_socket,const char *cmd_buffer){

	//..


	send_repl(client_socket,REPL_257);
	return 0;
}



int port(int client_socket, char * cmd_buff, char *client_add){  

	// get the port of data post
	//  ex.    cmd_buff  is   127,0,0,1,23,45
	//  the  client_add is   127.0.0.1
	//return 23*256+45;
	client_add[0]='\0';
	const char * result;
	int len=0,_toint=0,i=0;
	result=strtok(cmd_buff,PORTDELIM);	
	_toint=atoi(result);
	if (_toint<0 || _toint>254) return -1;
	strcat(client_add,result);
	len+=strlen(result);
	client_add[len]='.';
	len++;
	result=strtok(NULL,PORTDELIM);	
	_toint=atoi(result);
	if (_toint<0 || _toint>254) return -1;
	strcat(client_add,result);
	len+=strlen(result);
	client_add[len]='.';
	len++;

	result=strtok(NULL,PORTDELIM);	
	_toint=atoi(result);
	if (_toint<0 || _toint>254) return -1;
	strcat(client_add,result);
	len+=strlen(result);
	client_add[len]='.';
	len++;

	result=strtok(NULL,PORTDELIM);	
	_toint=atoi(result);
	if (_toint<0 || _toint>254) return -1;
	strcat(client_add,result);
	len+=strlen(result);
	client_add[len]='\0';
	
	int num1=0,num2=0;
	result = strtok(NULL,PORTDELIM);//23
	num1=atoi(result);
	result = strtok(NULL,PORTDELIM);//45
	num2=atoi(result);
	printf("#%s:%d\n",client_add,num1*256+num2);

	return num1*256+num2;
}

int ls(int client_socket,int data_socket,const char *dir){
	if(client_socket>0) {
		send_repl(client_socket,REPL_150);
	}
	else {
		close(client_socket);
		send_repl(client_socket,REPL_425);
		return -1;
	}
	//
	printf("ls ls ls ls ls ls ls");


	send_repl(client_socket,REPL_226);// finished
	return 0;

}

int get(int client_socket,int data_socket,const char *file_name,const char *file_dir){
	if(client_socket>0) {
		send_repl(client_socket,REPL_150);
	}
	else {
		close(client_socket);
		send_repl(client_socket,REPL_425);
		return -1;
	}

	//...
	printf("get get get get get get get get");


	send_repl(client_socket,REPL_226);// finished
	return 0;
}

int put(int client_socket,int data_socket,const char *file_name,const char *storePath){
	if(client_socket>0) {
		send_repl(client_socket,REPL_150);
	}
	else {
		close(client_socket);
		send_repl(client_socket,REPL_425);
		return -1;
	}

	char read_buff[DATE_BUFF_SIZE];
	int fpr = open(file_name,O_WRONLY|O_CREAT,0644);
	if(fpr<0) {
		close(data_socket);
		send_repl(client_socket,REPL_451);
		return -1;
	}

 	/*DataBlock dataBlock;
        do
        {
            recv(data_socket,&dataBlock,sizeof(DataBlock),0);
	    	cout<<"Total blocks: "<<dataBlock.blockTotalLen<<" , Recieved: "<<dataBlock.blockNO<<" blocks\n";
        }while(STORE(storePath,&dataBlock)!=EOF);*/

	
/*********************************************/
	while(1){
		
		int len = recv(data_socket,read_buff,DATE_BUFF_SIZE,0);
		if(len>0) {
			write(fpr,read_buff,len);
		}
		else {
			break;
		}
	}
	close(fpr);
/*********************************************/

	close(data_socket);
	send_repl(client_socket,REPL_226);// finished
	return 0;
}
