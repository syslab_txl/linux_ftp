#include <iostream>

#include "FileSystem.h"

using namespace std;

int isDir(char* path)
{
	struct stat st;
	stat(path,&st);
	if(S_ISDIR(st.st_mode))
		return 1;
	else
		return 0;
}

void printStat(struct stat* pStat)
{
	//st_dev , st_mode , st_uid , st_gid , st_size , st_mtime
	//printf("dev	mode	uid	gid	size	mtime\n");
	//printf("%ld , %d , %d , %d , %ld , %ld\n",pStat->st_dev,pStat->st_mode,pStat->st_uid,pStat->st_gid,pStat->st_size,pStat->st_mtime);
}

void List(char* path)
{
	if(isDir(path))
	{
		DIR* dir = opendir(path);
		if(!dir)
			return;
		

		struct dirent* fileD = NULL;
		printf("file 		---		type\n");
		printf("------------------------------------\n");
		while((fileD=readdir(dir))!=NULL)
		{
			printf("%s	---	%d\n",fileD->d_name,fileD->d_type);
		}
		closedir(dir);
	}
	else
	{
		struct stat st;
		stat(path,&st);
		printStat(&st);
	}
}

/*trans_mode Mode(trans_mode mode)
{
	trans_mode tmp = cur_mode;
	cur_mode = mode;
	return tmp;
}*/

/*string PWD()
{
	cout<<CUR_DIR<<endl;
}*/

//STORE
int STORE(char* path,DataBlock* data)
{
	int file;
	string filePath = path;
	filePath += ".tmp";
	if((file = fileIn(filePath.c_str()))==-1)
		file = addFile(filePath.c_str(),data->blockTotalLen);	
	//int file = open(path,O_WRONLY|O_APPEND|O_CREAT,S_IWUSR);
	if(file==-1)
		return LOCAL_ERR;
	if(write(file,static_cast<void*>(data->data),data->length) == -1)
		return LOCAL_ERR;
	return updateFile(file,data->blockNO);
}

DataPack* INI_FILE_BUFFER(char* path)
{
	int file = open(path,O_RDONLY,S_IWUSR);
	if(file==-1)
		return NULL;
	struct stat st;
	stat(path,&st);
	int fileSize = st.st_size;
	int bufferNum = fileSize/DATABLOCK_LEN;
	if(fileSize%DATABLOCK_LEN!=0)
		bufferNum++;
	DataPack* dataPack = new DataPack;
	if(dataPack==NULL)
		return NULL;
	dataPack->fileId = file;
	dataPack->fileSize = fileSize;
	dataPack->totalBlockNum = bufferNum;
	dataPack->segmentNum = dataPack->totalBlockNum/DATABLOCK_NUM_PER_SEGMENT;
	if(dataPack->totalBlockNum % DATABLOCK_NUM_PER_SEGMENT!=0)
		dataPack->segmentNum++;
	dataPack->restBlockNum = bufferNum;
	dataPack->bufferLen = 0;
	dataPack->data = NULL;
}

int READ_FILE_BUFFER(DataPack* dataPack)
{	
	if(dataPack==NULL)
		return LOCAL_ERR;
	if(dataPack->data!=NULL)
	{
		delete dataPack->data;
		dataPack->bufferLen = 0;
		dataPack->data = NULL;
	}
	if(dataPack->restBlockNum<=0)
		return EOF;
	int file = dataPack->fileId;
	int bufferNum = 0;
	if(dataPack->restBlockNum>=DATABLOCK_NUM_PER_SEGMENT)
		bufferNum = DATABLOCK_NUM_PER_SEGMENT;
	else
		bufferNum = dataPack->restBlockNum;
	DataBlock* dataBuffers = new DataBlock[bufferNum];
	int i=0,n=dataPack->totalBlockNum-dataPack->restBlockNum;
	for(;i<bufferNum;i++)
	{

		if(read(file,(dataBuffers+i)->data,DATABLOCK_LEN)==-1)
			return LOCAL_ERR;
		dataBuffers[i].length = DATABLOCK_LEN;
		
		dataBuffers[i].blockNO = n+1;
		n++;
		dataBuffers[i].blockTotalLen = dataPack->totalBlockNum;
	}
	
	dataPack->restBlockNum -= bufferNum;
	dataPack->bufferLen = bufferNum;
	dataPack->data = dataBuffers;
	
	//最后一个段落末尾1KB数据处理，截去多余的部分
	if(dataPack->restBlockNum == 0)
	{
		if(dataPack->totalBlockNum*DATABLOCK_LEN>dataPack->fileSize)
		{
			dataBuffers[--i].length = DATABLOCK_LEN - dataPack->totalBlockNum*DATABLOCK_LEN + dataPack->fileSize;
		}
	}	

	return SUCCESS;
}

int CLOSE_FILE_BUFFER(DataPack* dataPack)
{
	//delete[] dataPck->data;
	close(dataPack->fileId);
	delete dataPack;
}

/*int main()
{
	char path[100];
	char storePath[100];
	cout<<"File path :";
	cin>>path;
	cout<<"Store path :";
	cin>>storePath;
	//List(path);

	DataPack* dataPack = INI_FILE_BUFFER(path);
	if(dataPack==NULL)
	{
		printf("Failed to read data.\n");
		return 0;
	}
	cout<<"Segment Num: "<<dataPack->segmentNum<<endl;
	cout<<"Block Num: "<<dataPack->totalBlockNum<<endl;
	int CurSeg = 1;
	while(READ_FILE_BUFFER(dataPack)!=EOF)
	{
		cout<<"CurSegment: "<<CurSeg<<endl;
		for(int i=0;i<dataPack->bufferLen;i++)
		{
			STORE(storePath,dataPack->data+i);
		}
		CurSeg++;
	}
	CLOSE_FILE_BUFFER(dataPack);

	return 0;
}*/
